const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

function usersInterestedInPlayingVideoGames(users) {
    users = Object.entries(users)
    let result = users.filter(user => {
        let interests = user[1].interests || user[1].interest
        let intrestedInVideoGames = interests.some(interest => {
            return interest.includes("Video Games")
        })
        return intrestedInVideoGames
    })
        .reduce((output, user) => {
            output[user[0]] = user[1]
            return output
        }, {})
    return result
}
console.log(usersInterestedInPlayingVideoGames(users))

function allUsersStayingInGermany(users) {
    users = Object.entries(users)
    let result = users.filter(user => {

        return user[1].nationality === "Germany"
    })
        .reduce((output, user) => {
            output[user[0]] = user[1]
            return output
        }, {})
    return result

}
// console.log(allUsersStayingInGermany(users))

function sortUsersBasedOnSeniority(users) {
    let sortedUsers = Object.entries(users)
        .sort((user1, user2) => {
            return user2[1].age - user1[1].age
        })
        .map(user => {
            if (user[1].desgination.includes('Senior')) {
                user[1].desgination = [1, user[1].desgination]
            }
            else if (user[1].desgination.includes('Developer')) {
                user[1].desgination = [2, user[1].desgination]
            }
            else {
                user[1].desgination = [3, user[1].desgination]
            }
            return user
        })
        .sort((user1, user2) => {
            return user1[1].desgination[0] - user2[1].desgination[0]
        })
        .reduce((output, user) => {

            user[1].desgination = user[1].desgination[1]
            output[user[0]] = user[1]
            return output
        }, {})

    return sortedUsers


}
// console.log(sortUsersBasedOnSeniority(users))

function allUsersWithMastersDegree(users) {
    users = Object.entries(users)
    let result = users.filter(user => {

        return user[1].qualification === "Masters"
    })
        .reduce((output, user) => {
            output[user[0]] = user[1]
            return output
        }, {})
    return result
}
// console.log(allUsersWithMastersDegree(users))

function groupUsersBasedProgramming(users) {
    users = Object.entries(users)

    const programs = ['Python', 'Javascript', 'Golang']


    let programUser = programs.map(program => {
        let programUser = users.filter(user => {
            return user[1].desgination.includes(program)
        })

        return [program, programUser]
    })

    let result = programUser.reduce((output, user) => {
        user[1] = user[1].reduce((obj, user) => {
            obj[user[0]] = user[1]
            return obj
        }, {})
        output[user[0]] = user[1]

        return output
    }, {})


    return result
}
// console.log(groupUsersBasedProgramming(users))